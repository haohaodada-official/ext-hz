# 徽章板


![](./arduinoC/_images/featured.png)

---------------------------------------------------------

## 目录

* [描述](#描述)
* [积木列表](#积木列表)
* [使用介绍](#使用介绍)
* [使用方法1](#使用方法1)
* [示例程序1](#示例程序1)
* [使用方法2](#使用方法2)
* [示例程序2](#示例程序2)
* [许可证](#许可证)
* [支持列表](#支持列表)
* [更新记录](#更新记录)

## 描述
好好搭搭-徽章板：支持连接其他主控时作为从屏显示。

## 积木列表

![](./arduinoC/_images/blocks.png)

## 使用介绍

[徽章板快速使用视频教程](https://www.haohaodada.com/new/course_play.php?id=33&info_id=59)

徽章板串口和主控板要交叉连接。

![](./arduinoC/_images/connect.png)

## 使用方法1

[徽章板从屏固件下载](download.bin)

## 示例程序1

![](./arduinoC/_images/example1.png)

![](./arduinoC/_images/example2.png)

## 使用方法2

徽章板编写自定义程序

## 示例程序2

> 徽章板接收英文

![](./arduinoC/_images/example3.png)

![](./arduinoC/_images/hz3.png)

> 徽章板接收数字

![](./arduinoC/_images/example4.png)

![](./arduinoC/_images/hz4.png)

> 徽章板接收图案

![example5](./arduinoC/_images/example5.png)

![hz5](./arduinoC/_images/hz5.png)

## 许可证

MIT

## 支持列表

主板型号                | 实时模式    | ArduinoC   | MicroPython    | 备注
------------------ | :----------: | :----------: | :---------: | -----
arduino        |             |       √      |             | 
micro:bit        |             |       √       |             | 
mpython        |             |       √      |             | 


## 更新记录
* V0.0.1  基础功能完成
* V0.0.2  功能优化
* V0.0.3  说明完善
* V0.0.4  更新图案模块
* V0.0.5  完善图案模块

