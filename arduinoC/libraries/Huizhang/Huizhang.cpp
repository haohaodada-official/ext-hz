#include "Huizhang.h"
#include <SoftwareSerial.h>

HuiZhang::HuiZhang(uint8_t port0, uint8_t port)
{
    _HuiZhang_pin0 = port0;
    _HuiZhang_pin = port;
    #if defined(ESP_PLATFORM)
        Serial2.begin(19200, _HuiZhang_pin0, _HuiZhang_pin);
    #endif
}

void HuiZhang::write(String a, bool b)
{
    #if defined(ESP_PLATFORM)
        b ? Serial2.println(a) : Serial2.print(a);
        delayMicroseconds(28);
    #else
        #ifndef hz_Serial
            SoftwareSerial hz_Serial(_HuiZhang_pin0, _HuiZhang_pin);
            hz_Serial.begin(19200);
        #endif
        b ? hz_Serial.println(a) : hz_Serial.print(a);
        #if defined(NRF5)
            delayMicroseconds(24);//不同CPU延时时间mega328 (25)  Microbit (24)  M0和ESP32(28)
        #else
            delayMicroseconds(25);
        #endif
    #endif
}

// void HuiZhang::writehex(uint16_t a)
// {
//     #if defined(ESP_PLATFORM)
//         Serial1.begin(115200, P0, _HuiZhang_pin);
//         Serial1.print(a);
//     #else
//         #ifndef hz_Serial/
//             SoftwareSerial hz_Serial(3, _HuiZhang_pin);
//             hz_Serial.begin(115200);
//         #endif
//             hz_Serial.print(a);
//     #endif
//     delay(1);
// }

void HuiZhang::SendClr()
{
    write("hc",1);
}

void HuiZhang::SendNum(String a)
{
    write(String("hn"+a),1);
}

void HuiZhang::SendNumAtX(String a, uint8_t x)
{
    write("hn"+String(x)+"/"+a,1);
}

void HuiZhang::SendStr(String a)
{
    write("hs"+a,1);
}

void HuiZhang::SendStrAtX(String a, uint8_t x)
{
    write("hs"+String(x)+"/"+a,1);
}

void HuiZhang::SendMatrixDisplay()
{
    write("hd",1);
}

void HuiZhang::SendMatrix(uint16_t* c, int s)
{
    write("hx"+String(s)+"/",0);
    for (int i = 0; i < 11; i++)
    {
        write(tohex(c[i]), 0);
    }
    write("",1);
}

//十进制转十六进制
String HuiZhang::tohex(uint16_t n)
{
    if (n == 0) {
        return "0x000"; //n为0
    }
    String result = "";
    char _16[] = {
        '0', '1', '2', '3', '4', '5', '6', '7', '8', '9', 'a', 'b', 'c', 'd', 'e', 'f'
    };
    const int radix = 16;
    while (n) {
        int i = n % radix;        // 余数
        result = _16[i] + result; // 将余数对应的十六进制数字加入结果
        n /= radix;               // 除以16获得商，最为下一轮的被除数
    }
    if (result.length() < 2) {
        result = "0x00" + result; //不足两位补零
    } else if (result.length() < 3) {
        result = "0x0" + result; //不足三位补零
    } else {
        result = "0x" + result;
    }
    return result;
}