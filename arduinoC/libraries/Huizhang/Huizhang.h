#ifndef _HUIZHANG_H_
#define _HUIZHANG_H_
#include "Arduino.h"

class HuiZhang
{
public:
	HuiZhang(uint8_t port0, uint8_t port);
	void write(String, bool);
	void SendClr();
	void SendNum(String);
	void SendNumAtX(String, uint8_t);
	void SendStr(String);
	void SendStrAtX(String, uint8_t);
	void SendMatrixDisplay();
	void SendMatrix(uint16_t*, int);
	String tohex(uint16_t);
private:
	uint8_t _HuiZhang_pin0;
	uint8_t _HuiZhang_pin;
};
#endif
